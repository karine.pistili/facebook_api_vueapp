module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  devServer: { //enable https, still has to generate key and certificate
    open: process.platform === 'darwin',
    host: '0.0.0.0',
    port: 8080, // CHANGE YOUR PORT HERE!
    https: true,
    hotOnly: false,
  },
}