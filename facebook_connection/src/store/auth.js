export default {
    state:{
        loggedIn: false,
        loading: false
    },
    mutations:{
        setLoggedIn(state,payload){
            state.loggedIn = payload
        },
        setLoading(state,payload){
            state.loading = payload
        }
    }
}